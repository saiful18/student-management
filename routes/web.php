<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BatchController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller( TeacherController::class )->group( function () {
    Route::get( '/teacher', 'index' )->name( 'teacher.index' );
    Route::post( '/teacher', 'teacherRegistration' )->name( 'teacher.registration' );
} );
Route::controller( BatchController::class )->group( function () {
    Route::get( '/batch', 'create' )->name( 'batch.index' );
    Route::post( '/batch', 'batchCreate' )->name( 'batch.create' );
} );

Route::controller( RoleController::class )->group( function () {
    Route::get( '/role', 'create' )->name( 'role.index' );
    Route::post( '/role/store', 'store' )->name( 'role.store' );
} );

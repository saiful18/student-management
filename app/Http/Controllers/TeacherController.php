<?php

namespace App\Http\Controllers;

use App\Models\Teacher; //Model thaka Teacher newa hoycha
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view( 'backend.Relations.teacher' );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }


    public function teacherRegistration( Request $request ) {
        $request->validate( [
            'name' => 'required',
        ] );
        Teacher::create( [
            'name' => $request->name,
        ] );
        return redirect()->back();
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Teacher $teacher )
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Teacher $teacher )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Teacher $teacher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Teacher $teacher)
    {
        //
    }
}
